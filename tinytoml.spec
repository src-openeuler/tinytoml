#
# spec file for package tinytoml
#

%define debug_package %{nil}

Name: tinytoml
Version: 0.4
Release: 2
License: BSD-2-Clause
Summary: A header only C++11 library for parsing TOML.
Url: https://github.com/mayah/tinytoml
Source: %{name}-%{version}.tar.gz
BuildArch: noarch
# Patch:
# BuildRequires:

%description
The tinytoml-devel package includes the header files of tinytoml.

%package devel
Summary: Headers for tinytoml devel
# Requires:

%description devel
%{summary}.

%prep
%autosetup -n tinytoml-%{version}

%build
ls -l ./include
cp -r ./include %{buildroot}
ls -l %{buildroot}

%files devel
/toml

%changelog
* Wed Apr 10 2024 Chen Hongyu <chenhongyu1@huawei.com> - 0.4-2
- Update license

* Tue Sep 19 2023 Zhu Jiameng <zhujiameng@huawei.com> - 0.4-1
- update to 0.4-1

